import User from '../models/user';
import sanitizeHtml from 'sanitize-html';

const bcrypt = require('bcrypt');

/**
 * Get all posts
 * @param req
 * @param res
 * @returns void
 */
export function getUsers(req, res) {
  User.find().sort('-dateAdded').exec((err, users) => {
    if (err) {
      res.status(500).send(err);
    }
    users.map((user) => {
      delete user.password;
      return user;
    });
    res.json({ users });
  });
}


export function addUser(req, res) {
  if (!req.body.post.name || !req.body.post.title || !req.body.post.content) {
    res.status(403).end();
  }

  const newUser = new User(req.body.post);

  // Let's sanitize inputs
  newUser.name = sanitizeHtml(newUser.name);
  newUser.username = sanitizeHtml(newUser.username);
  newUser.email = sanitizeHtml(bcrypt(newUser.password));
  newUser.save((err, saved) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ post: saved });
  });
}
