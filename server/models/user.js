import mongoose from 'mongoose';
import cuid from 'cuid';

const Schema = mongoose.Schema;

const userSchema = new Schema({
  cuid: { type: 'String', default: cuid(), required: true },
  name: { type: 'String', required: true },
  username: { type: 'String', required: true },
  email: { type: 'String', required: true },
  password: { type: 'String', required: true },
  dateAdded: { type: 'Date', default: Date.now, required: true },
});

export default mongoose.model('User', userSchema);
